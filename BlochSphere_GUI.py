from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *
from Bloch_Sphere_Main import Ui_MainWindow
import sys,os,logging
from pathlib import Path

MANIM_DIRECTORY = Path(r"C:\Users\erdem\Desktop\3Blue1BrownRepo\manim")

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)
        
        # Compile preset animation
        self.compilePresetButton.clicked.connect(self.compilePreset)

        self.show()  
    
    # Procedure to compile
    @pyqtSlot()
    def compilePreset(self):
        path1 = MANIM_DIRECTORY / "manim.py"
        path2 = MANIM_DIRECTORY / "BlochSphere_Visualization.py"
        command_string = "python " + path1.absolute().as_posix() + " " + path2.absolute().as_posix()
        print(command_string)

        # Pauli X Gate
        if self.presetCombo.currentIndex() == 1:
            command_string += " PauliX"
        # Pauli Y Gate
        elif self.presetCombo.currentIndex() == 2:
            command_string += " PauliY"
        # Pauli Z Gate
        elif self.presetCombo.currentIndex() == 3:
            command_string += " PauliZ"
        # Hadamard Gate
        elif self.presetCombo.currentIndex() == 4:
            command_string += " HadamardRotate"
        elif self.presetCombo.currentIndex() == 5:
            command_string += " Hadamard_P45"
        elif self.presetCombo.currentIndex() == 6:
            command_string += " Hadamard_P90"
        elif self.presetCombo.currentIndex() == 7:
            command_string += " Hadamard_P180"
        elif self.presetCombo.currentIndex() == 8:
            command_string += " Hadamard_P270"
        elif self.presetCombo.currentIndex() == 9:
            command_string += " PauliX_X"
        elif self.presetCombo.currentIndex() == 10:
            command_string += " PauliY_Y"
        elif self.presetCombo.currentIndex() == 11:
            command_string += " PauliZ_Z"
        elif self.presetCombo.currentIndex() == 12:
            command_string += " Hadamard_2"
        elif self.presetCombo.currentIndex() == 13:
            command_string += " PauliX_Y"
        elif self.presetCombo.currentIndex() == 14:
            command_string += " PauliX_Z"
        elif self.presetCombo.currentIndex() == 15:
            command_string += " PauliY_X"
        elif self.presetCombo.currentIndex() == 16:
            command_string += " PauliY_Z"
        elif self.presetCombo.currentIndex() == 17:
            command_string += " PauliZ_X"
        elif self.presetCombo.currentIndex() == 18:
            command_string += " PauliZ_Y"
        elif self.presetCombo.currentIndex() == 19:
            command_string += " Hadamard_X"
        elif self.presetCombo.currentIndex() == 20:
            command_string += " Hadamard_Y"
        elif self.presetCombo.currentIndex() == 21:
            command_string += " Hadamard_Z"
        elif self.presetCombo.currentIndex() == 22:
            command_string += " Hadamard_X_H"
        elif self.presetCombo.currentIndex() == 23:
            command_string += " Hadamard_Y_H"
        elif self.presetCombo.currentIndex() == 24:
            command_string += " Hadamard_Z_H"
        
        else:
            print("Please select an animation to continue: ")
            return
        if self.checkBox_2.isChecked() == True:
            command_string += " -p"
            if self.checkBox.isChecked()==False:
                command_string += "l"
        elif self.checkBox.isChecked()==False:
            command_string += " -l"
        os.system(command_string)

if __name__ == '__main__':
    app = QApplication([])

    window = MainWindow()
    app.exec_()
            
        
          
        
