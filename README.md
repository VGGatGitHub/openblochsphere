# Welcome to OpenBlochSphere Project!
## This project is based on manim, an animation engine for explanatory math videos 
## NOTICE:
### Only the additional files have been added to this repository. In order to generate these animations on your own, you must clone [manim](https://github.com/3b1b/manim) or this [fork](https://github.com/dor2727/manim/tree/my_project) into your local drive and then clone this repository inside the "manim" directory 

![logo](logo/cropped.png)
[![Build Status](https://travis-ci.org/3b1b/manim.svg?branch=master)](https://travis-ci.org/3b1b/manim)
[![Documentation](https://img.shields.io/badge/docs-EulerTour-blue.svg)](https://www.eulertour.com/docs)
[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](http://choosealicense.com/licenses/mit/)
[![Manim Subreddit](https://img.shields.io/reddit/subreddit-subscribers/manim.svg?color=ff4301&label=reddit)](https://www.reddit.com/r/manim/)
[![Manim Discord](https://img.shields.io/discord/581738731934056449.svg?label=discord)](https://discord.gg/mMRrZQW)

Manim is an animation engine for explanatory math videos. It's used to create precise animations programmatically, as seen in the videos at [3Blue1Brown](https://www.3blue1brown.com/).

# Installation
Manim runs on Python 3.7. You can install it from PyPI via pip:
```sh
pip3 install manimlib
```

System requirements are [cairo](https://www.cairographics.org), [ffmpeg](https://www.ffmpeg.org), [sox](http://sox.sourceforge.net), [latex](https://www.latex-project.org) (optional, if you want to use LaTeX).

You can now use it via the `manim` command. For example:

```sh
manim my_project.py MyScene
```

## Directly (Windows)
1. [Install FFmpeg](https://www.wikihow.com/Install-FFmpeg-on-Windows).
2. [Install Cairo](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pycairo). For most users, ``pycairo‑1.18.0‑cp37‑cp37m‑win32.whl`` will do fine.
    ```sh
    pip3 install C:\path\to\wheel\pycairo‑1.18.0‑cp37‑cp37m‑win32.whl
    ```
3. Install a LaTeX distribution. [MiKTeX](https://miktex.org/download) is recommended.

4. [Install SoX](https://sourceforge.net/projects/sox/files/sox/).

5. Install the remaining Python packages. Make sure that ``pycairo==1.17.1`` is changed to ``pycairo==1.18.0`` in requirements.txt.
    ```sh
    git clone https://github.com/3b1b/manim.git
    cd manim
    pip3 install -r requirements.txt
    python3 manim.py example_scenes.py SquareToCircle -pl
    ```

## Anaconda Install

* Install sox and latex as above. 
* Create a conda environment using `conda env create -f environment.yml`
* **WINDOWS ONLY** Install `pyreadline` via `pip install pyreadline`. 

### Using `virtualenv` and `virtualenvwrapper`
After installing `virtualenv` and `virtualenvwrapper`
```sh
git clone https://github.com/3b1b/manim.git
mkvirtualenv -a manim -r requirements.txt manim
python3 -m manim example_scenes.py SquareToCircle -pl
```
# Installing PyQt5
In order to use the interface in this repository, you must install [pyQt5](https://pypi.org/project/PyQt5/) and [pyqt5-tools](https://pypi.org/project/pyqt5-tools/) 
You can install both from PyPI via pip:
```sh
pip install pyQt5
```
then
```sh
pip install pyqt5-tools
```


# Using manim
Try running the following:
```sh
python3 -m manim example_scenes.py SquareToCircle -pl
```
The `-p` flag in the command above is for previewing, meaning the video file will automatically open when it is done rendering. The `-l` flag is for a faster rendering at a lower quality.

Some other useful flags include:
* `-s` to skip to the end and just show the final frame.
* `-n <number>` to skip ahead to the `n`'th animation of a scene.
* `-f` to show the file in finder (for OSX).

Set `MEDIA_DIR` environment variable to specify where the image and animation files will be written.

Look through the `old_projects` folder to see the code for previous 3b1b videos. Note, however, that developments are often made to the library without considering backwards compatibility with those old projects. To run an old project with a guarantee that it will work, you will have to go back to the commit which completed that project.

While developing a scene, the `-sp` flags are helpful to just see what things look like at the end without having to generate the full animation. It can also be helpful to use the `-n` flag to skip over some number of animations.

# Bloch Sphere Visualization with manim
![logo](images/BlochSphere_Image.png)

* The Bloch Sphere implementation is based on this [fork](https://github.com/dor2727/manim/tree/my_project) of [manim](https://github.com/3b1b/manim).

* We need [qubit_utils.py](https://github.com/dor2727/manim/blob/my_project/my_project/qubit_utils.py) and [qubit.py](https://github.com/dor2727/manim/blob/my_project/my_project/qubit.py) in order to visualize the Bloch Sphere. 

* [qubit_bloch_sphere_examples.py](https://github.com/dor2727/manim/blob/my_project/my_project/qubit_bloch_sphere_examples.py) contains the classes of the quantum logic gate operations and use the objects defined in [qubit_utils.py](https://github.com/dor2727/manim/blob/my_project/my_project/qubit_utils.py) and [qubit.py](https://github.com/dor2727/manim/blob/my_project/my_project/qubit.py).

## Further Instructions:
### If you clone this [fork](https://github.com/dor2727/manim/tree/my_project), you can clone this repository and generate the animations straight away
### However, if you clone [manim](https://github.com/3b1b/manim), you must add [physics.py](physics.py) into the "mobject" folder inside "manimlib" and add the following code to "imports.py":
```sh
from manimlib.mobject.physics import *
``` 

# The User Interface for the Bloch Sphere Animation Generator
![logo](images/GUI_Image.png)

* In order to design the UI, [PyQ5t](https://pypi.org/project/PyQt5/)'s [Qt Designer](https://doc.qt.io/qt-5/qtdesigner-manual.html) was used.
* [Bloch_Sphere_GUI.ui](https://gitlab.com/qkitchen/openblochsphere/-/blob/master/Bloch_Sphere_GUI.ui) file contains the code needed to generate the interface.
* [BLoch_Sphere_Main.py](https://gitlab.com/qkitchen/openblochsphere/-/blob/master/Bloch_Sphere_Main.py) file was generated with PyQt5 UI code generator 5.13.0
* [BlochSphere_GUI.py](https://gitlab.com/qkitchen/openblochsphere/-/blob/master/BlochSphere_GUI.py) is used to program the actions of each QComboBox, QCheckBox and QPushButton in the interface. 
* [BlochSphere_Visualization.py](https://gitlab.com/qkitchen/openblochsphere/-/blob/master/BlochSphere_Visualization.py) is called within [BlochSphere_GUI.py](https://gitlab.com/qkitchen/openblochsphere/-/blob/master/BlochSphere_GUI.py) and contains the classes that generate the gate operations. 

## Running 

* In order to run the UI yourself, open the command line, go to the manim directory and type:
```sh
python BlochSphere_GUI.py
```

# Some Animations

## Pauli X 
![](media/PauliX.mp4)

## Pauli Y 
![](media/PauliY.mp4)

## Pauli Z 
![](media/PauliZ.mp4)

## Hadamard 
![](media/HadamardRotate.mp4)

## More animations in /[media](https://gitlab.com/qkitchen/openblochsphere/-/tree/master/media)

# Other Materials
## Here are some of the useful materials found when working on this project:

### Playing with the Bloch Sphere:
* [Medium Article](https://link.medium.com/D7ZN9krLf8)
* Built in Unity3D at IBM’s Emerging Tech Labs
* [GitHub Link](GitHub:https://github.com/GwilymNewton/BlochSphereExplorer-) 

### Bloch sphere representation of quantum states for a spin 1/2 particle:
* [University of St. Andrews](https://www.st-andrews.ac.uk/physics/quvis/simulations_html5/sims/blochsphere/blochsphere.html)

### QuTip(Quantum Toolbox in Python):
* [Plotting on the Bloch Sphere](http://qutip.org/docs/4.1/guide/guide-bloch.html)
* Both 2D and 3D visualizations are present with the classes qutip.Bloch and qutip.Bloch3d
* qutip.Bloch uses Matplotlib to render the Bloch sphere; qutip.Bloch3d uses the Mayavi rendering engine

### Bloch-Sphere-Visualization:
* [GitHub Link](https://github.com/tqdwyer/Bloch-Sphere-Visualization)
* Uses VPython for visualization and wxPython for GUI
* VPython can be used in a Jupyter Notebook and the 3D visualization runs in the same page, under the corresponding the cell 
* [YouTube Link](https://www.youtube.com/watch?v=qCtB8S7VG8U)

### Qubit Bloch-Sphere Visualization:
* [pyPI Link](https://pypi.org/project/bloch-sphere/)
* You can install it from PyPI via pip:
```sh
pip install bloch-sphere
```
* [Cairo](https://www.cairographics.org/) needs to be installed seperately to render videos
* [Animations](https://github.com/cduck/bloch_sphere/blob/master/examples/common_gates.md) for the common gates 


